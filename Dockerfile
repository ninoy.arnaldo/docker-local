FROM debian:latest

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y
RUN apt install -y bash vim

WORKDIR /usr/src/workspace
COPY . .

RUN echo ". /usr/src/workspace/entrypoint.sh" >> /root/.bashrc

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["./entrypoint.sh"]