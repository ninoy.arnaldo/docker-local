# docker-local

## Docker local
Minimal files to bootstrap docker based projects on local.

## Description
Bootstrap local development with docker.

## Usage
...TO DO...

## Support
For comments and/or suggestions, email me at [ninoy.arnaldo@gmail.com](mailto:ninoy.arnaldo@gmail.com).

## Project status
Will be active as long as docker is active.
